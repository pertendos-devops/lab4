package main

import (
	"fmt"
	"github.com/cenkalti/backoff/v4"
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
	"os"

	"github.com/jackc/pgx"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	log.Println("start")
	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	conn, err := initStore()
	if err != nil {
		log.Fatalf("failed to initialise the store: %s", err)
	}
	defer conn.Close()
	log.Println("connected to db")

	e.GET("/", func(c echo.Context) error {
		log.Println("get /")
		return rootHandler(conn, c)
	})

	e.GET("/ping", func(c echo.Context) error {
		log.Println("get ping")
		return c.JSON(http.StatusOK, struct{ Status string }{Status: "OK"})
	})

	e.POST("/send", func(c echo.Context) error {
		log.Println("post /send")
		return sendHandler(conn, c)
	})

	httpPort := os.Getenv("HTTP_PORT")
	if httpPort == "" {
		httpPort = "8080"
	}

	e.Logger.Fatal(e.Start(":" + httpPort))
}

type Message struct {
	Value string `json:"value"`
}

func initStore() (*pgx.Conn, error) {
	config, err := pgx.ParseConnectionString(os.Getenv("DATABASE_URL"))
	if err != nil {
		return nil, err
	}

	var (
		conn *pgx.Conn
		try  int64 = 0
	)
	openDB := func() error {
		try++
		log.Printf("try connect %d", try)
		conn, err = pgx.Connect(config)
		return err
	}

	err = backoff.Retry(openDB, backoff.NewExponentialBackOff())
	if err != nil {
		return nil, err
	}

	if _, err := conn.Exec(
		"CREATE TABLE IF NOT EXISTS message (value text PRIMARY KEY)"); err != nil {
		return nil, err
	}

	return conn, nil
}

func rootHandler(conn *pgx.Conn, c echo.Context) error {
	r, err := countRecords(conn)
	if err != nil {
		return c.HTML(http.StatusInternalServerError, err.Error())
	}
	return c.HTML(http.StatusOK, fmt.Sprintf("Hello, Docker! (%d)\n", r))
}

func sendHandler(conn *pgx.Conn, c echo.Context) error {
	m := &Message{}

	if err := c.Bind(m); err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	_, err := conn.Exec(
		"INSERT INTO message (value) VALUES ($1) ON CONFLICT (value) DO UPDATE SET value = excluded.value",
		m.Value,
	)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, m)
}

func countRecords(conn *pgx.Conn) (int, error) {
	rows, err := conn.Query("SELECT COUNT(*) FROM message")
	if err != nil {
		return 0, err
	}
	defer rows.Close()

	count := 0
	for rows.Next() {
		if err := rows.Scan(&count); err != nil {
			return 0, err
		}
		rows.Close()
	}

	return count, nil
}
